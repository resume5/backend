import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import morgan from 'morgan';
import user_controller from '@controllers/user'
import auth from './middleware/auth'
import bcrypt from 'bcrypt'
import Debug from "debug";
import bodyParser from 'body-parser'
import * as fs from "fs";
import * as path from "path";
import * as rfs from 'rotating-file-stream';

const MIDDLEWARE = [auth]
const {UserModel} = require("./models/user.model");
require('dotenv/config');

let serverLog: Debug.Debugger = Debug("Server")

class App {
    public appLogStream: fs.WriteStream
    public debug: Debug.Debugger = Debug("Server")
    public app: express.Application
    public Router;
    private accessLogStream: any;

    public constructor() {
        this.accessLogStream = rfs.createStream('routes.log', {
            size: "2M",
            interval: '3d', // rotate daily
            path: path.join(__dirname,'../logs')
        });
        this.app = express()
        this.Router = express.Router()

        App.dataBase()
        this.middleware()
        this.routes()
    }

    private middleware(): void {
        //use config module to get the privatekey, if no private key set, end the application
        if (!process.env.KEY) {
            console.error("FATAL ERROR: KEY is not defined.");
            process.exit(1);
        }
        this.app
            .use(express.json())
            .use(express.urlencoded({ extended: true }))
            .use(bodyParser.json())
            .use(cors())
            .use(morgan('dev'))
            .use(morgan('combined', { stream: this.accessLogStream}))
            .use(this.Router)
    }

    private static dataBase(): void {
        mongoose.set('useCreateIndex', true);
        mongoose
            .connect(`mongodb://mongo/${process.env.MONGO_INITDB_DATABASE}`, {useNewUrlParser: true, useUnifiedTopology: true })
            .then(() => serverLog("Connected to MongoDB..."))
            .catch(err => serverLog("Could not connect to MongoDB..."));
        createAdminMaster();
    }

    private routes(): void {
        this.Router
            .post("/api/sign-up", MIDDLEWARE, (req, res, next) => user_controller.sign_up(req, res, next))
            .post("/api/sign-in", (req, res, next) => user_controller.sign_in(req, res, next))
            .get("/api/auth", (req, res, next) => user_controller.auth(req, res, next))
            .get("/api/sign-out", (req, res, next) => user_controller.sign_out(req, res, next))
    }
}

function createAdminMaster() {
    // Verify if is created or create
    UserModel.findOne({email: process.env.ADMIN_EMAIL}).then(async document => {
        if (document) {
            serverLog('Admin MASTER already created...')
        } else {
            let user = new UserModel({
                first_name: process.env.FIRST_NAME,
                last_name: process.env.LAST_NAME,
                password: process.env.ADMIN_PASSWORD,
                email: process.env.ADMIN_EMAIL,
                isAdmin: process.env.ADMIN_ISADMIN
            });
            user.password = await bcrypt.hash(user.password, 10);
            await user.save();
            serverLog('Admin MASTER created !');
        }
    });
}

export default new App().app;
