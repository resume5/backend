const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

//simple schema
const UserSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50
    },
    last_name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 255
    },
    //give different access rights if admin or not
    isAdmin: Boolean
});


//custom method to generate authToken
UserSchema.methods.generateAuthToken = function() {
    //get the private key from the config file -> environment variable
    return jwt.sign({_id: this._id, isAdmin: this.isAdmin}, process.env.KEY);
}

const UserModel = mongoose.model('Users', UserSchema);


let validateUserCreation = (user) => {
    const schema = Joi.object({
        first_name: Joi.string().min(3).max(50).required(),
        last_name: Joi.string().min(3).max(50).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(3).max(255).required()
    });

    return schema.validate({first_name: user.first_name, last_name: user.last_name, email: user.email, password: user.password});
}

let validateUser = (user) => {
    const schema = Joi.object({
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(3).max(255).required()
    });

    return schema.validate({email: user.email, password: user.password});
}
exports.UserModel = UserModel;
exports.validateUser = validateUser;
exports.validateUserCreation = validateUserCreation;
