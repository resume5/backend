import {NextFunction, Request, Response} from "express";
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const {UserModel, validateUser, validateUserCreation} = require("../models/user.model");


class User {
    sign_in = async (req: Request, res: Response, next: NextFunction) => {
        // validate the request body first
        const {error} = validateUser(req.body.user);
        if (error) return res.status(400).send(error.details[0].message);

        //find an existing user
        let user = await UserModel.findOne({email: req.body.user.email});
        let isTrue = await bcrypt.compare(req.body.user.password, user.password);
        if (!isTrue) return res.status(400).send('Password is not valid !');

        let token = await jwt.sign({
            data: req.body.user.email
        }, process.env.APP_SECRET, {expiresIn: '15m'});

        res.cookie('authorization', token);

        return res.status(200).json({'isValid': true, 'email': req.body.user.email, 'token': token, "isAdmin": user.isAdmin});
    }
    sign_up = async (req: Request, res: Response, next: NextFunction) => {
        // validate the request body first
        const {error} = validateUserCreation(req.body.user);
        if (error) return res.status(400).send(error.details[0].message);

        //find an existing user
        let user = await UserModel.findOne({email: req.body.user.email});
        if (user) return res.status(400).send("User already registered.");

        user = new UserModel({
            name: req.body.user.name,
            password: req.body.user.password,
            email: req.body.user.email,
            isAdmin: req.body.user.isAdmin
        });
        user.password = await bcrypt.hash(user.password, 10);
        await user.save();

        res.send({
            _id: user._id,
            name: user.name,
            email: user.email
        });
    }

    async auth(req, res, next) {
        //get the token from cookies if present
        let token = req.headers.authorization.split(' ')[1]
        //if no token found, return response (without going to the next middelware)
        if (!token || token === 'null') return res.status(400).send("Access denied. No token provided.");


        await jwt.verify(token, process.env.APP_SECRET, async (err, decoded) => {
            if (err || !decoded.data) {
                res.status(302).json({'isValid': false, 'email': "No emails inside."});
            } else {
                req.token = await jwt.sign({
                    data: decoded.data
                }, process.env.APP_SECRET, {expiresIn: '15m'});
                res.cookie('authorization', req.token);
                try{
                    let user = await UserModel.findOne({email: decoded.data});
                    res.status(200).json({'isValid': true, 'email': decoded.data, 'token': req.token, "isAdmin": user.isAdmin})
                }catch (e){
                    console.log(e)
                }
                res.status(200).json({'isValid': true, 'email': decoded.data, 'token': req.token, "isAdmin": false})
            }
        });
    }

    sign_out(req, res, next) {
        res.cookie('authorization', '');
        res.status(200).redirect('/home')
    }
}
function getCookieToken(request) {
    let rc = request.headers.cookie;
    let token = 'No Token';
    rc && rc.split(';').forEach(function (cookie) {
        if (cookie.match(` authorization=`)) {
            token = cookie.replace(" authorization=", "");
        }else if (cookie.match(`authorization=`)){
            token = cookie.replace("authorization=", "");
        }
    });
    return token;
}
export default new User()
