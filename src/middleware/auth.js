const jwt = require("jsonwebtoken");

async function auth(req, res, next) {
    //get the token from cookies if present
    let token = req.headers.authorization.split(' ')[1]
    //if no token found, return response (without going to the next middelware)
    if (!token) return res.status(400).send("Access denied. No token provided.");

    if (!process.env.APP_SECRET) console.log("No token variable.")

    await jwt.verify(token, process.env.APP_SECRET, async (err, decoded) => {
        if (err) {
            res.status(302).redirect('/');
        } else {
            req.token = await jwt.sign({
                data: decoded.data
            }, process.env.APP_SECRET, {expiresIn: '15m'});
            res.cookie('authorization', req.token);
            next()
        }
    });
}

function getCookieToken(request) {
    let rc = request.headers.cookie;
    let token = 'No Token';
    rc && rc.split(';').forEach(function (cookie) {
        if (cookie.match(` authorization=`)) {
            token = cookie.replace(" authorization=", "");
        }else if (cookie.match(`authorization=`)){
            token = cookie.replace("authorization=", "");
        }
    });
    return token;
}

module.exports = auth
