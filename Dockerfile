FROM node:12.16.2-alpine as dependency
RUN apk --no-cache add curl bash && \
    curl -o- -L https://yarnpkg.com/install.sh | bash
WORKDIR /dependency
COPY package.json .
RUN yarn install

FROM node:12.16.2-alpine as build
ENV TZ=America/Sao_Paulo
WORKDIR /usr/app
COPY . .
COPY --from=dependency /dependency/node_modules ./node_modules
CMD ["yarn","run","start"]
EXPOSE 5500